import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './user';




@Injectable()
export class UserService {

    userUrl = "http://localhost:8089/quizwebservice/rest/token/user";
    public username: string;
    public firstname: string;
    public lastname: string;
    public token: string;
    private isLoginedUser: boolean=false;
    //Create constructor to get Http instance
    constructor(private http: HttpClient) { }


filterUsers(username: string, password: string): Observable<any> {
       let httpHeaders = new HttpHeaders()
                          .set('Accept', 'application/json');
            return this.http.get<User>(this.userUrl+"?name="+username+"&password="+password+"", {
                    headers: httpHeaders,
                    responseType: 'json'
            });


        }

        filterUsersUsingPost(username: string, password: string): Observable<any> {
            let httpHeaders = new HttpHeaders()
                               .set('Accept', 'application/json');
                 return this.http.post<any>(this.userUrl,{"name":username,"password":password}, {
                         headers: httpHeaders,
                         responseType: 'json'
                 }
                
                );
     
     
             }

             remedyUsersPost(): Observable<any> {
                let httpHeaders = new HttpHeaders()
                                   .set('Accept', 'application/json');
                     return this.http.post<any>("http://localhost:8008/api/jwt/login?username=akshay.a&password=1",{}, {
                             headers: httpHeaders,
                             responseType: 'json'
                     }
                    
                    );
         
         
                 }

             createSubmit(dec: string,impact: string,urgency: string,status: string,reported_source: string,service_type: string){

                this.remedyUsersPost().subscribe(data => { 
                    this.token=data['token']
                }

                );
                

                let httpHeaders = new HttpHeaders();
                httpHeaders.append('Accept', 'application/json').append('Authorization value','AR-JWT '+this.token);

                                       
                 return this.http.post<any>("http://localhost:8008/api/arsys/v1/entry/HPD:IncidentInterface_Create"
                    , {"First_Name" : this.firstname ,
                 "Last_Name" : this.lastname ,
                "Description" : dec ,
                 "Impact" : impact ,
                 "Urgency" : urgency ,
                 "Status" : status ,
                 "Reported Source" : reported_source ,
                 "Service_Type" : service_type ,
                 "z1D_Action" : "CREATE"}, {
                         headers: httpHeaders,
                         responseType: 'json'
                 }
                
                );
     
             }



setUserLogin(){
    this.isLoginedUser=true;
}

unSetUserLogin(){
    this.isLoginedUser=false;
}

getUserLogin(){
    return this.isLoginedUser;
}

    
   

   
    }