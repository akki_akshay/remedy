import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes }   from '@angular/router';






import { UserService } from './user.service';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { ViewComponent } from './view/view.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { AuthGuardService } from './auth-guard.service';


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'main', component: MainComponent,canActivate: [AuthGuardService] },
  { path: 'new', component: HomeComponent,canActivate: [AuthGuardService]},
  { path: 'view', component: ViewComponent,canActivate: [AuthGuardService] }
];

@NgModule({
 
  imports: [
  
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    FilterPipeModule,
    HttpClientModule,
    NgxPaginationModule,
    RouterModule.forRoot(routes)
   ],

  declarations: [
    AppComponent,
    HomeComponent,
    MainComponent,
    LoginComponent,
    ViewComponent
    
  ],

  providers: [UserService,AuthGuardService],
 /* providers: [],*/
  bootstrap: [AppComponent]
})
export class AppModule { }
