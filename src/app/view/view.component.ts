import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  
  services: any[] = [
    { 'Company': 'prodapt', 'Customer': 'CD456', 'Impact': 'service request','Urgency':'ACEG','Priority':'high','Status':'request','InstanceId':'1234' },
    { 'Company': 'prodapt', 'Customer': 'CD456', 'Impact': 'service request','Urgency':'ACEG','Priority':'high','Status':'request','InstanceId':'1234' },
    { 'Company': 'prodapt', 'Customer': 'CD456', 'Impact': 'service request','Urgency':'ACEG','Priority':'high','Status':'request','InstanceId':'1234' },
    { 'Company': 'prodapt', 'Customer': 'CD456', 'Impact': 'service request','Urgency':'ACEG','Priority':'high','Status':'request','InstanceId':'1234' },
    { 'Company': 'prodapt', 'Customer': 'CD456', 'Impact': 'service request','Urgency':'ACEG','Priority':'high','Status':'request','InstanceId':'1234' },
    { 'Company': 'prodapt', 'Customer': 'CD456', 'Impact': 'service request','Urgency':'ACEG','Priority':'high','Status':'request','InstanceId':'1234' },
    { 'Company': 'prodapt', 'Customer': 'CD456', 'Impact': 'service request','Urgency':'ACEG','Priority':'high','Status':'request','InstanceId':'1234' },
    { 'Company': 'prodapt', 'Customer': 'CD456', 'Impact': 'service request','Urgency':'ACEG','Priority':'high','Status':'request','InstanceId':'1234' }
  ];

  userFilter: any= { 'incident_id': '' };

}
